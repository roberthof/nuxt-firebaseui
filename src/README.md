# Nuxt Developer Test

## Build Setup

```bash
# install dependencies
$ npm install

# initialize firebase in parent directory
$ firebase init

# insert correct firebase credentials
$ cp plugins/firebase.config.js.dist plugins/firebase.config.js

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
