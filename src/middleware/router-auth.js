export default function({ store, redirect, route }) {
  const user = store.state.user;
  const protectedRoute = /\/dashboard\/*/g;
  const homeRoute = "/";

  if (!user && route.path.match(protectedRoute)) {
    redirect("/");
  }

  if (user && route.path === homeRoute) {
    redirect("/dashboard");
  }

}
