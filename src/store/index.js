import { fireAuth } from '@/plugins/firebase.js'

export const strict = false

export const state = () => ({
  user: null,
})

export const mutations = {
  SET_USER(state, payload) {
    state.user = payload;
  }
}

export const actions = {
  signOut({ commit }) {
    fireAuth
      .signOut()
      .then(() => {
        commit('SET_USER', null);
        this.$router.push('/');
      })
      .catch(err => alert(err))
  }
}
