module.exports = {
  mode: 'universal',
  target: 'server',
  head: {
    title: 'ExtraServings Canada',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'ExtraServings: Skip the Lines. Get Rewarded. Get a bonus of 20% or more at your favourite restaurants.' }
    ],
    link: [
      { rel: 'icon', type: 'image/png', href: '/favicon-32x32.png' }
    ]
  },
  build: {
    extractCSS: true,
  },
  buildModules: [],
  css: [],
  components: true,
  modules: [
    'bootstrap-vue/nuxt',
    '@nuxtjs/pwa',
  ],
  // https://bootstrap-vue.org/docs#icons
  bootstrapVue: {
    icons: true,
    // components: ['BContainer', 'BRow', 'BCol', 'BFormInput', 'BButton', 'BTable', 'BModal'],
    // directives: ['VBModal', 'VBPopover', 'VBTooltip', 'VBScrollspy']
  },

  plugins: [
    '~/plugins/fireauth.js',
    
  ],
  router: {
    middleware: 'router-auth'
  }
}
